// Postfiksnya.cpp : Defines the entry point for the console application.
//
// Author: Zenkovich Stanislav

#include "stdafx.h"
#include "HW1.h"

// 2e300

/*
s - sin
c- cos
t - tg
z - ctg
e - exp
*/

void push(List *&pF, List *p) // ���������� ������� � ����
{
	// ������� � ������ ���������� ������
	p->pNext = pF;
	pF = p;
}

List *pop(List *&pF) // ���������� ������� �� ����� (�� ������ ������)
{ // ��������� ������ ������� �� ������
	if (pF == 0) return 0;
	List *p = pF;
	pF = pF->pNext;
	return p;
}

int getRang(char Ch) // �������� ��������� ��������
{
	if (Ch == 's' || Ch == 'c' || Ch == 't' || Ch == 'e' || Ch == 'z') return 3;	// ��������� � ������� ������ ����
	if (Ch == '*' || Ch == '/') return 2;
	if (Ch == '+' || Ch == '-') return 1;
	else return 0; // ��� ������
}

void add(List *&pF, List *p) // �������� ������� � ����� ������
{ // �������� �������� ����� ������ (�������)
	p->pNext = 0;
	if (pF == 0)
	{
		pF = p;
		return;
	}
	List *pE = pF; // ����� ���������� ��������
	while(pE->pNext) pE = pE->pNext;
	pE->pNext = p;
}

List 
	*pStek = 0,	// ����
	*pQueue = 0; // �������

#ifdef DEBUG
void print(List *pP,char *str)
{
	List *p;

	p=pP;
	printf("%s:",str);
	while(p)
	{
		if (p->item.type)
		{
			printf("%c ", p->item.type);
		}
		else printf("%.2f ", p->item.val);
		p = p->pNext;
	}
	printf("\n");
}
#endif

int jGetChar()
{
	if (index <= strlen(text))
		return text[index++];
	else
		return '\n';
}

void JUnGetc()
{
	index--;
}

double calc(double MyX)
{
	pQueue = 0;
	pStek = 0;
	char Ch;
	char str[2] = "";
	double x;
	List *p, *p1;
	int fl = 1; // �������� ������� �����
	
	while(1)
	{
		Ch = jGetChar();// ������ ������� �� ������
		if (Ch == '\n') break;
		if ((Ch == '-' && fl) || Ch == '.' || (Ch >= '0' && Ch <= '9') || tolower(Ch) == 'x') // ������� �����
		{ // ������� ���� ����� (�������)
			JUnGetc(); // ������� ������ ������� � �����
			p=new List; // ����� �������

			if (tolower(Ch) == 'x')
			{
				p->item.type = 'x';
				sscanf(text+sizeof(char)*index,"%c",&x);
				index++;
			}
			else
			{
				p->item.type = 0; // ��� �����
				sscanf(text+sizeof(char)*index,"%lf", &x);// ������ ������������� ��������
				p->item.val = x; // ��������
				
				while (1)
				{
					Ch = jGetChar();
					if ((Ch == '.') || (Ch >= '0' && Ch <= '9'))
						;
					else
					{
						JUnGetc();
						break;
					}

				}
			}

			add(pQueue, p);// �������� � ������� (���. ������������������)
			fl=0; // ������� ����� ��������� ���� �� �����
			continue;
		}
		if (Ch == '-' || Ch == '+' || Ch == '*' || Ch == '/' || tolower(Ch) == 's' || tolower(Ch) == 'c' || tolower(Ch) == 't' || tolower(Ch) == 'e')
		{ // ������� ���� ��������
		
			while(1) // ���� ���������� ��������� �� �����
			{
				int rang = 0;
				// ���� ���� �� ���� �������� ���� �������� �������
				if (pStek) rang = getRang(pStek->item.type);

				if (rang < getRang(Ch) || !pStek)
				{   // ������� ������� � ��������� ��� � ����
					p = new List;

					// ��������� �� ������� �������
					if (tolower(Ch) == 's')	// �������� �����
					{
						sscanf(text+sizeof(char)*index,"%c%c",&str[0],&str[1]);
						if (tolower(str[0]) == 'i' && tolower(str[1]) == 'n')
						{
							p->item.type = 's';
							index+=2;
						}
						else
						{
							sprintf(state,"Error - invalid operand! (%c%c%c)",Ch,str[1],str[2]);
							return 2e300;
						}
					}
					else
					if (tolower(Ch) == 'c')	// �������� ������� ��� ���������
					{
						sscanf(text+sizeof(char)*index,"%c%c",&str[0],&str[1]);
						if (tolower(str[0]) == 'o' && tolower(str[1]) == 's')
						{
							p->item.type = 'c';
							index+=2;
						}
						else
						if (tolower(str[0]) == 't' && tolower(str[1]) == 'g')
						{
							p->item.type = 'z';
							index+=2;
						}
						else
						{
							sprintf(state,"Error - invalid operand! (%c%c%c)",Ch,str[0],str[1]);
							return 2e300;
						}
					}
					else
					if (tolower(Ch) == 't')	// �������� �������
					{
						sscanf(text+sizeof(char)*index,"%c",&str[0]);
						if (tolower(str[0]) == 'g')
						{
							p->item.type = 't';
							index+=2;
						}
						else
						{
							sprintf(state,"Error - invalid operand! (%c%c)",Ch,str[0]);
							return 2e300;
						}
					}
					else
					if (tolower(Ch) == 'e') // �������� exp
					{
						sscanf(text+sizeof(char)*index,"%c%c",&str[0],&str[1]);
						if (tolower(str[0]) == 'x' && tolower(str[1]) == 'p')
						{
							p->item.type = 'e';
							index+=2;
						}
						else
						{
							sprintf(state,"Error - invalid operand! (%c%c%c)",Ch,str[0],str[1]);
							return 2e300;
						}
					}
					else	// ����� ��������
					{
						p->item.type = Ch;
					}
					push(pStek, p);
					break;
				}
				// ������� ��������� �� �����
				// � ��������� ��� � �������
				p = pop(pStek);
				add(pQueue, p);
			}
			fl = 1; // ��������� ����� ���� ����. �����
			continue;
		}
		if (Ch == '(')
		{
			// ������ �������� � ����
			p = new List;
			p->item.type = Ch;
			push(pStek, p);
			fl = 1;// ��������� ����� ���� ����. �����
			continue;
		}
		if (Ch == ')')
		{
			if (!pStek)
			{
				sprintf(state,"Error - the stack is empty! (%c)",Ch);
				return 2e300;
			}
			// ���� ���������� ��������� �� (
			while(pStek)
			{
				p = pop(pStek);
				if (p->item.type != '(')
					add(pQueue, p);
				else 
				{
					delete p;
					p = 0;
					break;
				}

			}
			if (p) // ������ ������ �� �������
			{
				printf("Error - element ')' - no found!");
				return 2e300;
			}
			fl = 0;// ��������� �� ����� ���� ����. �����

			continue;
		}
	} // ����� ����� ������ ���� ������������������
	// ��������� ��� �� ����� � ���������� � �������
	while(pStek)
	{
		p = pop(pStek);
		add(pQueue, p);
	}
	char sq[80] = "";
	// ���� ������ ����������� �����
	p = pQueue;
	while(p)
	{
		if (p->item.type)
		{
			sprintf(sq, "%s%c ", sq,p->item.type);
		}
		else
			sprintf(sq, "%s%.2f ", sq,p->item.val);
		p = p->pNext;
	}
	// ����� ��������� ����������
	// �� ����������� �����

	while(pQueue)
	{
#ifdef DEBUG
		print(pQueue,"\nQueue");
		print(pStek,"Stek");
#endif
		p = pop(pQueue);
			
		if (p->item.type == 'x')
		{
			p->item.val = MyX;
			p->item.type = 0;
		}

		if (p->item.type == 0) // ������� ����� 
			push(pStek, p);
		else // ������� ���� ��������
		{
			double x, y, Rez;

			if (pStek == 0)
			{
				sprintf(state,"Error - the stack is empty!! (1) RPN: %s",sq);
				return 2e300;
			}


			p1 = pop(pStek);
			if (p1->item.type) // ������� �� �����
			{				
				sprintf(state,"Error - Operand is not number!! (%c)",p1->item.type);
				return 2e300;
			}
#ifdef DEBUG
			sprintf(state,"y: %c:%lf\n",p1->item.type,p1->item.val);
#endif
			y = p1->item.val;
			delete p1;
#ifdef DEBUG
			sprintf(state,"\n*** %c ***\n",p->item.type);
#endif
			// 
			if (pStek == 0 && 
				p->item.type != 's' && p->item.type != 'c' && p->item.type != 't' && p->item.type != 'z'  && p->item.type != 'e')
			{
				sprintf(state,"Error - the stack is empty!! (2) RPN: %s",sq);
				return 2e300;
			}

			if (pStek != 0 && 
				p->item.type != 's' && p->item.type != 'c' && p->item.type != 't' && p->item.type != 'z'  && p->item.type != 'e')
			{
				p1 = pop(pStek);
				if (p1->item.type && 
					p->item.type != 's' && p->item.type != '�' && p->item.type != 't' && p->item.type != 'z'  && p->item.type != 'e') // ������� �� �����
				{				
					sprintf(state,"Error - Operand is not number!! (%c)",p1->item.type);
					return 2e300;
				}
#ifdef DEBUG
				sprintf(state,"x: %c:%lf\n",p1->item.type,p1->item.val);
#endif
				x = p1->item.val;
				delete p1;
			}
			
			switch(p->item.type)
			{
			case '+': Rez = x+y; break;
			case '-': Rez = x-y; break;
			case '*': Rez = x*y; break;
			case '/': 
				if (y)	// �� ��������� ������� �� ����..
				{
					Rez = x/y; 
					break;
				}
				else
				{
					sprintf(state,"Divide by zero!");
					return 2e301;
				}
			case 's': Rez = sin(y); break;
			case 'c': Rez = cos(y); break;
			case 't': 
				if (cos(y))	// �� ��������� ������� �� ����..
				{
					Rez = sin(y)/cos(y);
					break;
				}
				else
				{
					sprintf(state,"Divide by zero!");
					return 2e301;
				}
			case 'z': 
				if (sin(y))	// �� ��������� ������� �� ����..
				{
					Rez = cos(y)/sin(y);
					break;
				}
				else
				{
					sprintf(state,"Divide by zero!");
					return 2e301;
				}
			case 'e': Rez = exp(y); break;
			default:
				sprintf(state,"Error!!! RPN: %s",sq);
				return 2e300;
			}
			p->item.type = 0;
			p->item.val = Rez;
			push(pStek, p);
		}
	}

	return (pStek->item.val);
}

