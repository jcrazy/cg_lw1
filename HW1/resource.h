//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HW1.rc
//
#define IDC_MYICON                      2
#define IDD_HW1_DIALOG                  102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_HW1                         107
#define IDI_SMALL                       108
#define IDC_HW1                         109
#define IDR_MAINFRAME                   128
#define IDD_EXPDIALOG                   129
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define IDC_COMBO1                      1003
#define IDC_COMBOR                      1003
#define IDD_EXPDEALOG                   32771
#define IDM_EXPDEALOG                   32772
#define IDM_EXPDIALOG                   32773
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
