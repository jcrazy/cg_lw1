// draw.cpp: ���������� ������� ���������
//
// Author: Zenkovich Stanislav

#include "stdafx.h"
#include "HW1.h"

/*
4 �������, � ������� ������� ���� �������
������� ����������, ��������� ������� �� ��������� �������������
���� ���� �� ��� X. �� ������ ��������� ��������� �����.
������ �������� ��������� ��������� ��� �����.
�� ���� �� ����� �� ���������� ������� �������-�������� �������.
�� ������ ������� �������� 2 �� ��� � �� ��� �������� � ������.
1. ����������� ������� ����� ����� ����������.
2. ���������. kx+b (��������� ��������� � � �� ������ ������������ �
����� �� ��� � ��� Y)
3. ���������� 4� �������, �������������.
4. ���������� 8�� �������, �������������.
*/

void drawLineEqu(HDC hdc, double x1, double y1, double x2, double y2, COLORREF color) {

	double k = (y2 - y1) / (x2 - x1);	// ������� ���������� ������
	double b = (x2 * y1 - x1 * y2) / (x2 - x1);	// �������� ������

	//MoveToEx(hdc,x1,y1,NULL);
	
	for (int i = x1; i < x2; i++)
	{
		LineTo(hdc,i,k*i+b);
	}
}

// ���������������� ������� �� ���������
void drawLine(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color) {
    const int deltaX = abs(x2 - x1);
    const int deltaY = abs(y2 - y1);
    const int signX = x1 < x2 ? 1 : -1;
    const int signY = y1 < y2 ? 1 : -1;
    //
    int error = deltaX - deltaY;
    //
    SetPixel(hdc, x2, y2, color);
    while(x1 != x2 || y1 != y2) {
        SetPixel(hdc, x1, y1, color);
        const int error2 = error * 2;
        //
        if(error2 > -deltaY) {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX) {
            error += deltaX;
            y1 += signY;
        }
    }
}

// ���������������� ������� �� ��������� (4� �������)
// � ������ 4-� ��������� �� ������ ���� �� �������� �� ������� ������ ���� �� ��������� ������� �����
void drawLine4x(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color) {
    const int deltaX = abs(x2 - x1);
    const int deltaY = abs(y2 - y1);
    const int signX = x1 < x2 ? 1 : -1;
    const int signY = y1 < y2 ? 1 : -1;
    //
    int error = deltaX - deltaY;
    //
    SetPixel(hdc, x2, y2, color);
    while(x1 != x2 || y1 != y2) {
        SetPixel(hdc, x1, y1, color);
        const int error2 = error * 2;
        //
        if(error2 > -deltaY) {
            error -= deltaY;
            x1 += signX;
        }
		else	// � ����� ��� � ������� (;
        if(error2 < deltaX) {
            error += deltaX;
            y1 += signY;
        }
    }
}

// ���������������� ������� �� ��������� (8�� �������)
// � ������ 8-�� ��������� �� ����� ������ ����� � ������������ � �������������� ���������� ������� �����, �� ����� �� ����� ��� �� �������
void drawLine8x(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color) {
    const int deltaX = abs(x2 - x1);
    const int deltaY = abs(y2 - y1);
    const int signX = x1 < x2 ? 1 : -1;
    const int signY = y1 < y2 ? 1 : -1;
    //
    int error = deltaX - deltaY;
    //
    SetPixel(hdc, x2, y2, color);
    while(x1 != x2 || y1 != y2) {
        SetPixel(hdc, x1, y1, color);
        const int error2 = error * 2;
        //
        if(error2 > -deltaY) {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX) {
            error += deltaX;
            y1 += signY;
        }
    }
}