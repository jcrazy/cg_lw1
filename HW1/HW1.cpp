// HW1.cpp: ���������� ����� ����� ��� ����������.
//
// Author: Zenkovich Stanislav

// ��������!!!
// ����� ���������� ������� � ������� ���������� ������ ����� �������!!!

// ����� ��� ����� ������� ;-)

#include "stdafx.h"
#include "HW1.h"

#define MAX_LOADSTRING 100

// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	ExpBox(HWND, UINT, WPARAM, LPARAM);

// ��������� ����������
const int Y0 = 250;
const int X0 = 275;

int jDraw = 0;	// ��������� ���������: 1 - ������������, 0 - ���; ����� ���� � ������������ bool
TCHAR Ttext[80];	// ���-�� ��� �����������
char text[80];	// ����������, ���������� ������ ���������
TCHAR Tstate[80];	// ��������� ���������� ��� �����������(?)
char state[80];	// ���������
int index = 0;	// ��������� ���������� ������� �� ������
TCHAR scale[20];	// �������
char ts[20] = "";	// �� �����
int s = 10;	// �� �����
int MAX = 250;	// ������� ���������� ��������� � �������
int iMax = 0;	// ��������� ���������� ��������� � �������
int typeDraw = 0;	// ��� ��������� �������

Point *iPoint;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_HW1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_HW1));

	// ���� ��������� ���������:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
//  �����������:
//
//    ��� ������� � �� ������������� ���������� ������ � ������, ���� �����, ����� ������ ���
//    ��� ��������� � ��������� Win32, �� �������� ������� RegisterClassEx'
//    ������� ���� ��������� � Windows 95. ����� ���� ������� ����� ��� ����,
//    ����� ���������� �������� "������������" ������ ������ � ���������� �����
//    � ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HW1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_HW1);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
      CW_USEDEFAULT, 0, 550, 600, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND	- ��������� ���� ����������
//  WM_PAINT	-��������� ������� ����
//  WM_DESTROY	 - ������ ��������� � ������ � ���������.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_ABOUT:	// ���������� ���� "� ���������"
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXPDIALOG:	// ���������� ���� "����������"
			DialogBox(hInst, MAKEINTRESOURCE(IDD_EXPDIALOG), hWnd, ExpBox);
			InvalidateRect(hWnd,0,TRUE);	// ��������� ����
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CHAR:
		char key;

		key = wParam;
		// ������������ �������
		if (key == '+')
			if (s < 100) s++;
		if (key == '-')
			if (s > 1) s--;
		InvalidateRect(hWnd,0,TRUE);
		break;
	case WM_MOUSEMOVE:
		if (jDraw)
		{
			InvalidateRect(hWnd,0,TRUE);
			jDraw = 0;
		}
		else
			InvalidateRect(hWnd,0,FALSE);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		{
			// �����
			
			SetTextColor(hdc, RGB(0, 255, 0));

			// ���� ���� ��������� ��� ������, �� �������
			if (strlen(state))
			{
				TextOut(hdc, 5, 25, state, strlen(state));
			}
			
			char q[90];
			char drawTypeText[80];
			TCHAR Tq[90];
			if (strlen(text))
			{
				// ��� ������� (����� ������..)
				
				// ������� ����� ���������
				sprintf(q,"Exp: %s",text);
				TextOut(hdc, 5, 5, q, strlen(q));

				// ���������� ��� ���������..
				switch (typeDraw)
				{
				case 0:
					strcpy(drawTypeText,"LineTo on WinAPI");
					break;
				case 1:
					strcpy(drawTypeText,"SetPixel on WinAPI");
					break;
				case 2:
					strcpy(drawTypeText,"LineTo on WinAPI (y=kx+b)");
					break;
				case 3:
					strcpy(drawTypeText,"LineTo on Bresenham's line algorithm x4");
					break;
				case 4:
					strcpy(drawTypeText,"LineTo on Bresenham's line algorithm x8");
					break;

				}
				// .. � ������� ���
				TextOut(hdc, 5, 25, drawTypeText, strlen(drawTypeText));
			}		

			// ���������� �������� ..
			sprintf(q,"Scale: %d",s);
			TextOut(hdc, 450, 525, q, strlen(q));
			// .. � ���������� ����� � �������
			sprintf(q,"Point: %d",MAX);
			TextOut(hdc, 25, 525, q, strlen(q));

			// ���
			    HPEN pen1;
				COLORREF color1,color2,color3;
				color1=RGB(255,0,0); /* ���� - ������� */
				color2=RGB(0,0,255); /* ���� - ����� */
				color3=RGB(0,0,0); /* ���� - ������ */

				// ����� ����
				pen1=CreatePen(PS_SOLID,1,color2);
				SelectObject(hdc,pen1);
			    MoveToEx(hdc,X0,475,NULL);
				LineTo(hdc,X0,25);
				MoveToEx(hdc,50,Y0,NULL);
				LineTo(hdc,500,Y0);
				DeleteObject(pen1);
				pen1=CreatePen(PS_SOLID,1,color3);
				SelectObject(hdc,pen1);
				// �������
				for (int i = 0; i <= 225; i+=s*5)
				{
					MoveToEx(hdc,X0-5,Y0+i,NULL);
					LineTo(hdc,X0+5,Y0+i);
				}
				for (int i = 0; i <=225; i+=s*5)
				{
					MoveToEx(hdc,X0-5,Y0-i,NULL);
					LineTo(hdc,X0+5,Y0-i);
				}
				for (int i = 0; i <= 225; i+=s*5)
				{
					MoveToEx(hdc,X0+i,Y0-5,NULL);
					LineTo(hdc,X0+i,Y0+5);
				}
				for (int i = 0; i <= 225; i+=s*5)
				{
					MoveToEx(hdc,X0-i,Y0-5,NULL);
					LineTo(hdc,X0-i,Y0+5);
				}
				DeleteObject(pen1);

			// ���� ���� ��� ��������, ��..
			if (strlen(text) && iPoint)
			{	

				MoveToEx(hdc,X0+(iPoint[0].x*s),Y0+(iPoint[0].y*s),NULL);
				pen1=CreatePen(PS_SOLID,1,color1);
				SelectObject(hdc,pen1);

				for (int i = 0; i < MAX; i++)
				{
					if (iPoint[i].y == 2e300)
					{
						sprintf(state,"Attention: The function has a point of the cliff!");
					}
					else
					if (X0+iPoint[i].x*s <= 550 && X0+iPoint[i].x*s >= 0 && Y0+iPoint[i].y*s >= 0 && Y0+iPoint[i].y*s <= 550)
					{
						// � ����������� �� ���� ��������� �������� ����������� �������
						switch (typeDraw)
						{
						case 0:	// LineTo on WinAPI
							LineTo(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s));
							break;
						case 1:	// SetPixel on WinAPI
							SetPixel(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s),color1);
							break;
						case 2:	// LineTo on WinAPI (y=kx+b)
							drawLineEqu(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s),X0+(iPoint[i+1].x*s),Y0+(iPoint[i+1].y*s),color1);
							break;
						case 3:	// LineTo on Bresenham's line algorithm x4
							drawLine4x(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s),X0+(iPoint[i+1].x*s),Y0+(iPoint[i+1].y*s),color1);
							break;
						case 4:	// LineTo on Bresenham's line algorithm x8
							drawLine8x(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s),X0+(iPoint[i+1].x*s),Y0+(iPoint[i+1].y*s),color1);
							break;
						default: // �� ������ ������
							LineTo(hdc,X0+(iPoint[i].x*s),Y0+(iPoint[i].y*s));				
						}
					}		
				}

				DeleteObject(pen1);
			}
		}
		ReleaseDC(hWnd, hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK ExpBox(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	HWND hCBR = GetDlgItem(hDlg, IDC_COMBOR);
	
	switch (message)
	{
	case WM_INITDIALOG:
		{
			// ��������� ComboBox ���������� ������. ����� ���� ���������� ��������� ���������� ����� #define
			SendMessage(hCBR, CB_ADDSTRING, 0, (LPARAM)_T("LineTo on WinAPI"));
			SendMessage(hCBR, CB_ADDSTRING, 0, (LPARAM)_T("SetPixel on WinAPI"));
			SendMessage(hCBR, CB_ADDSTRING, 0, (LPARAM)_T("LineTo on WinAPI (y=kx+b)"));
			SendMessage(hCBR, CB_ADDSTRING, 0, (LPARAM)_T("LineTo on Bresenham's line algorithm x4"));
			SendMessage(hCBR, CB_ADDSTRING, 0, (LPARAM)_T("LineTo on Bresenham's line algorithm x8"));
			// ��������������� ��������� ����� �����
			SendMessage(hCBR, CB_SETCURSEL, typeDraw, 0);
			// ������� ������� ��� �������������� ��������� � ���������� �����
			SetWindowText(GetDlgItem(hDlg, IDC_EDIT1),text);
			
			char points[20];

			sprintf(points,"%d",MAX);
			SetWindowText(GetDlgItem(hDlg, IDC_EDIT2), points);
		}
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		// ��� ������������ �������	
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			if (LOWORD(wParam) == IDOK)	// � ������ ������ ������ ������ ��
			{
				/*if (iPoint != NULL)
				{
					// ������-�� �� ����� �������� ������� :(
					for (int i = 0; i < MAX; i++)
						free(&iPoint[i]);
					free(iPoint);
				}*/
				jDraw = 1;	// ��, ����� ��� �������� ������
				// �������� ��������� � �����
				GetDlgItemText(hDlg,IDC_EDIT1,Ttext,80);
				GetDlgItemText(hDlg,IDC_EDIT2,scale,20);
				sprintf(ts,"%s",scale);
				if (strlen(ts))
					MAX = atoi(ts);
				int HALF = MAX / 2;
				if ((MAX < 50) && (MAX > 500))
					MAX = 250;
				{
					if (iMax != MAX)	// ���� ���������� ����� � ������� ����������, �� ������� ��� ������
					{
						iPoint = NULL;	// �������� ���������
						iPoint = (Point *)calloc(MAX,sizeof(Point));
						iMax = MAX;
					}
					sprintf(text,"%s",Ttext);	

					double i = -(HALF/2);
					double j = 0;
					// ��������� ������ ������������ ����������
					// ��� ����� ���� �� ��������� ��������� �������:
					// ����� ����������� ��������� ��� ���������� � ���������� ��� ������ ��� ����������� ����������
					while (j < MAX)
					{				
						double res = 0;
						index=0;
						strcpy(state,"");
						res = (-calc(i));	// ���������� ���� ����������

						/*if (res = 2e302)
							break;*/
						if (res == 2e299)
						{
							continue;
						}
						
						// �����-�� ��������, ��� �� �����
						j = (HALF + i*2)-1;
						if ((j-3) >= HALF*2) break;
						// ���������� ������� ������������
						iPoint[(int)j].y = res;
						iPoint[(int)j].x = i;	
						i+=0.5; // ���������� ��� � ���� ��� ����������. ������ - ������.						
					}
				}
				typeDraw = SendMessage(hCBR, CB_GETCURSEL, 0, 0);	// ���������� ��������� ��� ���������
			}
			
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
